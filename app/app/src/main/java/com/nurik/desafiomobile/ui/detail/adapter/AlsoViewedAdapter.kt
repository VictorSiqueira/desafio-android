package com.nurik.desafiomobile.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.databinding.ItemAlsoviewedListBinding

class AlsoViewedAdapter(private val list: List<Produto>
) : RecyclerView.Adapter<AlsoViewedAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder =
        ProductsViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_alsoviewed_list,
                parent, false
            ))


    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        var prod = list[position]
        holder.recyclerViewBinding.product = prod
    }

    inner class ProductsViewHolder(val recyclerViewBinding: ItemAlsoviewedListBinding)
        : RecyclerView.ViewHolder(recyclerViewBinding.root)
}
