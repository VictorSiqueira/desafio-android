package com.nurik.desafiomobile.ui.products

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.network.ViaVarejoAPI
import com.nurik.desafiomobile.data.pojo.Produto
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ProductsViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var api: ViaVarejoAPI
    private lateinit var repository: ProductsRepository
    private lateinit var viewModel: ProductsViewModel


    @Before
    fun setup() {
        api = ViaVarejoAPI()
        repository = ProductsRepository(api)
        viewModel = ProductsViewModel(repository)
    }

    @Test
    fun `when LiveData is modified then emitting change`() {
        viewModel.setProductList(listOf(
            Produto(1, "nome")))
        assertEquals(1, viewModel.products.value?.size)

        viewModel.setProductList(listOf(
            Produto(1, "nome"),
            Produto(2, "nome"),
            Produto(3, "nome")))
        assertEquals(3, viewModel.products.value?.size)

        viewModel.setProductList(listOf(
            Produto(1, "nome"),
            Produto(2, "nome")))
        assertEquals(2, viewModel.products.value?.size)
    }
}