package com.nurik.desafiomobile.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.nurik.desafiomobile.R
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        homeViewModel.version.observe(this, Observer {
            version_name.text = StringBuilder()
                .append(getString(R.string.version_name))
                .append(" (")
                .append(it)
                .append(")")
                .toString()
        })
        homeViewModel.versionCode.observe(this, Observer {
            version_code.text = StringBuilder()
                .append(getString(R.string.version_code))
                .append(" (")
                .append(it)
                .append(")")
                .toString()
        })
        return inflater.inflate(R.layout.fragment_home, container, false)
    }
}