package com.nurik.desafiomobile.ui.home


import android.app.Application
import android.os.Build
import org.junit.Assert.*
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class HomeViewModelTest{

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private var application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
    private val viewModel: HomeViewModel by lazy { HomeViewModel(application) }

    @Test
    fun `when HomeViewModel is called then return app version` (){
        assertEquals("1.0", viewModel.version.value)
    }

    @Test
    fun `when HomeViewModel is called then return app version code` (){
        assertEquals("1", viewModel.versionCode.value)
    }
}
