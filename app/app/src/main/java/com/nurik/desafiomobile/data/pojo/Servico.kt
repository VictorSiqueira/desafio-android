package com.nurik.desafiomobile.data.pojo

data class Servico(
    val idLojista: Int,
    val nome: String,
    val parcelamento: String,
    val preco: Int,
    val sku: Int,
    val tipo: String
)