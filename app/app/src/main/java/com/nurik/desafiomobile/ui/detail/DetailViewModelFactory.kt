package com.nurik.desafiomobile.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.ui.products.ProductsViewModel

@Suppress("UNCHECKED_CAST")
class DetailViewModelFactory(
        private val repository: ProductsRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailViewModel(repository) as T
    }
}