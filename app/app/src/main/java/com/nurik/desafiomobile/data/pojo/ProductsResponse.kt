package com.nurik.desafiomobile.data.pojo

data class ProductsResponse(
    val produtos: List<Produto>,
    val quantidade: Int
)