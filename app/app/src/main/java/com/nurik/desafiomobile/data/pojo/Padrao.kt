package com.nurik.desafiomobile.data.pojo

data class Padrao(
    val disponivel: Boolean,
    val imagens: List<Imagem>,
    val marketplace: Marketplace,
    val nome: String,
    val preco: Preco,
    val servicos: List<Servico>,
    val sku: Int
)