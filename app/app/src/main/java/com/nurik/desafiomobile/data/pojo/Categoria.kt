package com.nurik.desafiomobile.data.pojo

data class Categoria(
    val descricao: String,
    val id: Int
)