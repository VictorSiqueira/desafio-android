package com.nurik.desafiomobile.data.pojo

data class Produto(
    val id: Int,
    val nome: String,
    var descricao: String?=null,
    var categorias: List<Categoria>?=null,
    var maisInformacoes: List<Informacao>?=null,
    var marca: Marca?=null,
    var modelo: Modelo?=null,
    var retiraEmLoja: Boolean?=null,
    var urlVideo: Any?=null,
    var classificacao: Double?=null,
    var disponivel: Boolean?=null,
    var imagemUrl: String?=null,
    var preco: Preco?=null,
    val precoAnterior: Double?=null,
    val precoAtual: Int?=null,
    val parcelamento: String?=null,
    val percentualCompra: Int?=null,
    var sku: Int?=null
)