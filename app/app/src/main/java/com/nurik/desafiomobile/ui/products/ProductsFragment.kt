package com.nurik.desafiomobile.ui.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.network.ViaVarejoAPI
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.ui.products.adapter.ProductAdapter
import com.nurik.desafiomobile.utils.ItemClickListener
import com.nurik.desafiomobile.utils.MiddleDividerItemDecoration
import kotlinx.android.synthetic.main.fragment_products.*

class ProductsFragment : Fragment(), ItemClickListener {
    private lateinit var factoryList: ProductsViewModelFactory
    private lateinit var productsViewModel: ProductsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        initViewModel()
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        productsViewModel.getProductList()
    }

    private fun initViewModel(){
        initFactory()
        productsViewModel = ViewModelProviders.of(this, factoryList).get(ProductsViewModel::class.java)
        productsViewModel.products.observe(this, Observer { list ->
            recycler_view.also {
                it.setHasFixedSize(true)
                it.layoutManager = GridLayoutManager(context, 2)
                it.adapter = ProductAdapter(list, this)
                it.addItemDecoration(MiddleDividerItemDecoration(context!!, DividerItemDecoration.HORIZONTAL))
                it.addItemDecoration(MiddleDividerItemDecoration(context!!, DividerItemDecoration.VERTICAL))
            }
        })
    }

    private fun initFactory() {
        val api = ViaVarejoAPI()
        val repository = ProductsRepository(api)
        factoryList = ProductsViewModelFactory(repository)
    }

    override fun <T : Any> onItemClickListener(item: T) {
        val prod = item as Produto
        val controller = findNavController()
        val bundle = Bundle()
        bundle.putDouble("classificacao", prod.classificacao!!)
        controller.navigate(R.id.nav_detail,bundle)
    }
}