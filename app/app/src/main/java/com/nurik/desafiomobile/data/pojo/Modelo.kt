package com.nurik.desafiomobile.data.pojo

data class Modelo(
    val padrao: Padrao,
    val skus: List<Any>
)