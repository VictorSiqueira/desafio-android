package com.nurik.desafiomobile.ui.home

import android.app.Application
import androidx.core.content.pm.PackageInfoCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class HomeViewModel(app: Application) : AndroidViewModel(app) {

    val packInfo = app.packageManager.getPackageInfo(app.packageName, 0)

    private val _version = MutableLiveData<String>().apply {
        value = packInfo.versionName
    }
    val version: LiveData<String> = _version

    private val _versionCode = MutableLiveData<String>().apply {
        value = PackageInfoCompat.getLongVersionCode(packInfo).toString()
    }
    val versionCode: LiveData<String> = _versionCode
}