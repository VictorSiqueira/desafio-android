package com.nurik.desafiomobile.data.pojo

data class Imagem(
    val altura: Int,
    val id: Int,
    val largura: Int,
    val nome: String,
    val url: String
)