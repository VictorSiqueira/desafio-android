package com.nurik.desafiomobile.data.pojo

data class Preco(
    val descontoFormaPagamento: DescontoFormaPagamento,
    val planoPagamento: String,
    val porcentagemDesconto: Int,
    val precoAnterior: Double,
    val precoAtual: Int,
    val quantidadeMaximaParcelas: Int,
    val valorParcela: Double
)