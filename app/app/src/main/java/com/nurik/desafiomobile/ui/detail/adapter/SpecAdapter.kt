package com.nurik.desafiomobile.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.pojo.Especificacao
import com.nurik.desafiomobile.databinding.ItemSpecListBinding

class SpecAdapter(private val list: List<Especificacao>
) : RecyclerView.Adapter<SpecAdapter.SpecViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpecViewHolder =
        SpecViewHolder(
            DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_spec_list,
            parent, false
        ))

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: SpecViewHolder, position: Int) {
        var spec = list[position]
        holder.recyclerViewBinding.spec = spec
    }

    inner class SpecViewHolder(val recyclerViewBinding: ItemSpecListBinding)
        : RecyclerView.ViewHolder(recyclerViewBinding.root)
}
