package com.nurik.desafiomobile.data.pojo

data class DescontoFormaPagamento(
    val descricao: Any,
    val porcentagemDesconto: Int,
    val possuiDesconto: Boolean,
    val preco: Int
)