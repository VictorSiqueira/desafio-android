package com.nurik.desafiomobile.data

import com.nurik.desafiomobile.data.network.SafeApiRequest
import com.nurik.desafiomobile.data.network.ViaVarejoAPI

class ProductsRepository(private val api: ViaVarejoAPI) : SafeApiRequest() {

    suspend fun getProductList() = apiRequest { api.getProductList()}

    suspend fun getProductDetail() = apiRequest { api.getProductDetail()}

    suspend fun getAlsoViewed() = apiRequest { api.getAlsoViewed()}
}