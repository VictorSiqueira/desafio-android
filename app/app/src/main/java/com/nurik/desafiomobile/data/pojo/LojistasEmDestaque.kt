package com.nurik.desafiomobile.data.pojo

data class LojistasEmDestaque(
    val compraOnline: Boolean,
    val eleito: Boolean,
    val id: Int,
    val nome: String,
    val preco: Int,
    val retiraRapido: Boolean
)