## Desenvolvedor Android

Este desafio foi criado para avaliar os candidatos as vagas de Android.

#### Desafio

Criar um aplicativo que contenha as seguintes telas:

- Home;
- Listagem de produtos;
- Detalhe do produto.

##### Requisitos

- Utilizar os mocks disponibilizados no documento para construção das telas;
- Utilizar Kotlin como linguagem de programação;
- Compatibilidade com API 19+;
- Realizar testes unitários;
- Realizar testes instrumentados.

##### Bibliotecas sugeridas

- Constraint Layout;
- Retrofit;
- Rx;
- Glide;
- Junit;
- Espresso.

##### O que será avaliado

- Lógica de programação;
- Conhecimento em Kotlin;
- Conhecimento em componentização;
- Utilização do Git;
- Testes unitários;
- Testes instrumentados.

##### Mocks

[Lista de Produtos](http://www.mocky.io/v2/5d1b4f0f34000074000006dd)
[Detalhe do Produto](http://www.mocky.io/v2/5d1b4fd23400004c000006e1)
[Quem viu comprou](http://www.mocky.io/v2/5d1b507634000054000006ed)

##### Protótipos

[Figma](https://www.figma.com/file/DLkwbGtUe0yDJPtcUxoqh0S5/Via-Varejo-Dev-Android?node-id=0%3A1)

###### Prints

![](./images/1.Home.png)
![](./images/2.Navigation.png)
![](./images/3.Lista_Produtos.png)
![](./images/4.Detalhe_Produto.png)

##### Observações

Criar uma documentação em markdown (Readme) explicando como as aplicações devem ser executadas, quais os pre-requisitos para a mesma.
No final do Readme criar uma sessão chamada Resultado e descrever todos os príncipais desafios, linhas de raciocínio e tempo gasto para execução do projeto.
