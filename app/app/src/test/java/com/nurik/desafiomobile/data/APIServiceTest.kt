package com.nurik.desafiomobile.data

import com.nurik.desafiomobile.data.pojo.ProductsResponse
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.data.network.ViaVarejoAPI
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test

class APIServiceTest {
    private lateinit var mockWebServer : MockWebServer
    private lateinit var api: ViaVarejoAPI
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")


    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        api = ViaVarejoAPI()
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `when getProductList is called then return ProductsResponse`() = runBlocking<Unit> {
        launch(Dispatchers.Main) {
            var response = api.getProductList()
            assertEquals("is returning ProductsResponse", true, response.body() is ProductsResponse)
            validateProductListResponse(response.body())
        }
    }

    fun validateProductListResponse(response: ProductsResponse?){
        assertEquals("is products different of null", true, response?.produtos!=null)
        assertEquals("is quantidade field differente of null", true, response?.quantidade!=null)
        assertEquals("is products field a List", true, response?.produtos is List<Produto>)
        assertEquals("is products List bigger then 0", 1, response?.produtos?.size?.compareTo(0))

        assertEquals("product item have id", true, response?.produtos?.get(0)?.id!=null)
        assertEquals("product item have sku", true, response?.produtos?.get(0)?.sku!=null)
        assertEquals("product item have nome", true, response?.produtos?.get(0)?.nome!=null)
        assertEquals("product item have disponivel", true, response?.produtos?.get(0)?.disponivel!=null)
        assertEquals("product item have descricao", true, response?.produtos?.get(0)?.descricao!=null)
        assertEquals("product item have imagemUrl", true, response?.produtos?.get(0)?.imagemUrl!=null)
        assertEquals("product item have classificacao", true, response?.produtos?.get(0)?.classificacao!=null)
        assertEquals("product item have preco", true, response?.produtos?.get(0)?.preco!=null)
    }

    @Test
    fun `when getProductDetail is called then return Produto`() = runBlocking<Unit> {
        launch(Dispatchers.Main) {
            var response = api.getProductDetail()
            assertEquals("is returning Produto", true, response.body() is Produto)
            validateProdutoDetailResponse(response.body())
        }
    }

    fun validateProdutoDetailResponse(produto: Produto?){
        assertEquals("produto have a id", true, produto?.id!=null)
        assertEquals("produto have a nome", true, produto?.nome!=null)
        assertEquals("produto have a descricao", true, produto?.descricao!=null)
        assertEquals("produto have a retiraEmLoja", true, produto?.retiraEmLoja!=null)
        assertEquals("produto have a categorias", true, produto?.categorias!=null)
        assertEquals("produto have a maisInformacoes", true, produto?.maisInformacoes!=null)
        assertEquals("produto have a marca", true, produto?.marca!=null)
        assertEquals("produto have a modelo", true, produto?.modelo!=null)
        assertEquals("produto don't have a urlVideo", true, produto?.urlVideo==null)
    }

    @Test
    fun `when getAlsoViewed is called then return Produto List`() = runBlocking<Unit> {
        launch(Dispatchers.Main) {
            var response = api.getAlsoViewed()
            assertEquals("is returning ProductsResponse", true, response.body() is List<Produto>)
            validateAlsoViewedListResponse(response.body())
        }
    }

    fun validateAlsoViewedListResponse(list: List<Produto>?){
        assertEquals("is products different of null", true, list?.get(0)!=null)

        assertEquals("product item have id", true, list?.get(0)?.id!=null)
        assertEquals("product item have sku", true, list?.get(0)?.sku!=null)
        assertEquals("product item have nome", true, list?.get(0)?.nome!=null)
        assertEquals("product item have imagemUrl", true, list?.get(0)?.imagemUrl!=null)
        assertEquals("product item have precoAtual", true, list?.get(0)?.precoAtual!=null)
        assertEquals("product item have precoAnterior", true, list?.get(0)?.precoAnterior!=null)
        assertEquals("product item have percentualCompra", true, list?.get(0)?.percentualCompra!=null)
        assertEquals("product item have classificacao", true, list?.get(0)?.classificacao!=null)
        assertEquals("product item have parcelamento", true, list?.get(0)?.parcelamento!=null)
    }
}