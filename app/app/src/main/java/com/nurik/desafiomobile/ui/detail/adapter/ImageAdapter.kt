package com.nurik.desafiomobile.ui.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.pojo.Imagem
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.databinding.ItemAlsoviewedListBinding
import com.nurik.desafiomobile.databinding.ItemImagemListBinding

class ImageAdapter(private val list: List<Imagem>
) : RecyclerView.Adapter<ImageAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder =
        ProductsViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_imagem_list,
                parent, false
            ))


    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        var prod = list[position]
        holder.recyclerViewBinding.imagem = prod
    }

    inner class ProductsViewHolder(val recyclerViewBinding: ItemImagemListBinding)
        : RecyclerView.ViewHolder(recyclerViewBinding.root)
}
