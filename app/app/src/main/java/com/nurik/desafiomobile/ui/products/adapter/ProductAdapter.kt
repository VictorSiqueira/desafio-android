package com.nurik.desafiomobile.ui.products.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.databinding.ItemProductListBinding
import com.nurik.desafiomobile.utils.ItemClickListener

class ProductAdapter(private val list: List<Produto>,
                     private val listener: ItemClickListener
) : RecyclerView.Adapter<ProductAdapter.ProductsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsViewHolder =
        ProductsViewHolder(
            DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_product_list,
            parent, false
        ))

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ProductsViewHolder, position: Int) {
        var prod = list[position]
        holder.recyclerViewBinding.product = prod
        holder.recyclerViewBinding.root.setOnClickListener{
            listener.onItemClickListener(prod)
        }
    }

    inner class ProductsViewHolder(val recyclerViewBinding: ItemProductListBinding)
        : RecyclerView.ViewHolder(recyclerViewBinding.root)
}
