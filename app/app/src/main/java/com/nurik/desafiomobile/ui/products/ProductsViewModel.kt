package com.nurik.desafiomobile.ui.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.utils.Coroutines
import kotlinx.coroutines.Job

class ProductsViewModel(private val repository: ProductsRepository) : ViewModel() {

    private lateinit var job: Job

    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }

    private val mProducts = MutableLiveData<List<Produto>>()
    val products: LiveData<List<Produto>>
        get() = mProducts

    fun setProductList(list: List<Produto>?){
        mProducts.value = list
    }

    fun getProductList(){
        job = Coroutines.ioThenMain(
            {repository.getProductList()},
            {setProductList(it?.produtos)})
    }

}