package com.nurik.desafiomobile.data.pojo

data class Informacao(
    val descricao: String,
    val valores: List<Especificacao>
)