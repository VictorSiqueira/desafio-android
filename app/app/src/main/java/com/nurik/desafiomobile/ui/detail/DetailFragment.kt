package com.nurik.desafiomobile.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nurik.desafiomobile.MainActivity
import com.nurik.desafiomobile.R
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.network.ViaVarejoAPI
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.databinding.FragmentProductDetailBinding
import com.nurik.desafiomobile.ui.detail.adapter.AlsoViewedAdapter
import com.nurik.desafiomobile.ui.detail.adapter.ImageAdapter
import com.nurik.desafiomobile.ui.detail.adapter.SpecAdapter
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.details_container_also_viewed.*
import kotlinx.android.synthetic.main.details_container_specs.*
import kotlinx.android.synthetic.main.fragment_product_detail.*

class DetailFragment : Fragment() {
    private lateinit var detailViewModel: DetailViewModel
    private lateinit var factory: DetailViewModelFactory
    private lateinit var binding: FragmentProductDetailBinding

    private val classificacao by lazy {
        arguments?.getDouble("classificacao")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        initViewModel()
        binding = inflate(inflater,R.layout.fragment_product_detail, container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailViewModel.getAlsoViewedList()
        detailViewModel.getProduto()
    }

    private fun initViewModel() {
        initFactory()
        detailViewModel = ViewModelProviders.of(this, factory).get(DetailViewModel::class.java)
        detailViewModel.alsoViewed.observe(this, Observer {
            detail_alsoViewed_recyclerview.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL ,false)
            detail_alsoViewed_recyclerview.adapter =
                AlsoViewedAdapter(it)
        })
        detailViewModel.produto.observe(this, Observer {
            var prod = detailViewModel.produto.value
            binding.product = prod
            setProdIntoUI(prod)
            (activity as MainActivity).setTitle(prod?.nome)
        })
    }

    private fun setProdIntoUI(prod: Produto?) {
        setupSpecList(prod)
        setupImageList(prod)
        detail_review_average.text = classificacao.toString()
    }

    private fun setupImageList(prod: Produto?) {
        if (prod?.modelo != null) {
            detail_image.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            detail_image.adapter = ImageAdapter(prod.modelo!!.padrao.imagens)
        }
    }

    private fun setupSpecList(prod: Produto?) {
        if (prod?.maisInformacoes != null) {
            detail_spec_recyclerview.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            detail_spec_recyclerview.adapter = SpecAdapter(prod!!.maisInformacoes!![0].valores)
        }
    }

    private fun initFactory() {
        val api = ViaVarejoAPI()
        val repository = ProductsRepository(api)
        factory = DetailViewModelFactory(repository)
    }
}