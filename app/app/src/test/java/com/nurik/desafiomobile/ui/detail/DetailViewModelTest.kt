package com.nurik.desafiomobile.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.network.ViaVarejoAPI
import com.nurik.desafiomobile.data.pojo.Produto
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class DetailViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var api: ViaVarejoAPI
    private lateinit var repository: ProductsRepository
    private lateinit var viewModel: DetailViewModel

    @Before
    fun setup() {
        api = ViaVarejoAPI()
        repository = ProductsRepository(api)
        viewModel = DetailViewModel(repository)
    }

    @Test
    fun `when AlsoViewed LiveData is modified then emitting change`() {
        viewModel.setAlsoViewed(listOf(
            Produto(1, "nome")))
        assertEquals(1, viewModel.alsoViewed.value?.size)

        viewModel.setAlsoViewed(listOf(
            Produto(1, "nome"),
            Produto(2, "nome"),
            Produto(3, "nome")))
        assertEquals(3, viewModel.alsoViewed.value?.size)

        viewModel.setAlsoViewed(listOf(
            Produto(1, "nome"),
            Produto(2, "nome")))
        assertEquals(2, viewModel.alsoViewed.value?.size)
    }

    @Test
    fun `when Produto LiveData is modified then emitting change`() {
        viewModel.setProduto(Produto(1, "nome"))
        assertEquals(1, viewModel.produto.value?.id)

        viewModel.setProduto(Produto(3, "nome"))
        assertEquals(3, viewModel.produto.value?.id)

        viewModel.setProduto(Produto(2, "nome"))
        assertEquals(2, viewModel.produto.value?.id)
    }
}