package com.nurik.desafiomobile.data.pojo

data class Especificacao(
    val nome: String,
    val valor: String
)