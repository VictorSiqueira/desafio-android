package com.nurik.desafiomobile.ui.home

import android.app.Application
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
import androidx.test.platform.app.InstrumentationRegistry
import com.nurik.desafiomobile.ui.home.HomeViewModel
import junit.framework.Assert.assertEquals
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeViewModelInstrumentedTest{

    @Test
    fun when_HomeViewModel_is_called_then_initiate_liveData (){
        val application = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as Application
        runOnUiThread(Runnable {
            var viewModel = HomeViewModel(application)
            assertEquals("1.0", viewModel.version.value)
            assertEquals("1", viewModel.versionCode.value)
        })
    }
}
