package com.nurik.desafiomobile.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nurik.desafiomobile.data.ProductsRepository
import com.nurik.desafiomobile.data.pojo.Produto
import com.nurik.desafiomobile.utils.Coroutines
import kotlinx.coroutines.Job

class DetailViewModel(val repository: ProductsRepository) : ViewModel() {
    private lateinit var job: Job

    override fun onCleared() {
        super.onCleared()
        if(::job.isInitialized) job.cancel()
    }

    private val mAlsoViewed = MutableLiveData<List<Produto>>()
    val alsoViewed: LiveData<List<Produto>>
        get() = mAlsoViewed

    fun setAlsoViewed(list: List<Produto>?){
        mAlsoViewed.value = list
    }

    fun getAlsoViewedList(){
        job = Coroutines.ioThenMain(
            {repository.getAlsoViewed()},
            {setAlsoViewed(it)})
    }

    private val mProduto = MutableLiveData<Produto>()
    val produto: LiveData<Produto>
        get() = mProduto

    fun setProduto(value: Produto?){
        mProduto.value = value
    }

    fun getProduto(){
        job = Coroutines.ioThenMain(
            {repository.getProductDetail()},
            {setProduto(it)})
    }

}