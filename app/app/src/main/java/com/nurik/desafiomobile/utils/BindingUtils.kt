package com.nurik.desafiomobile.utils

import android.graphics.Paint
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("image")
fun loadImage(view: ImageView, url:String?){
    Glide.with(view).load(url).into(view)
}

@BindingAdapter("oldprice")
fun setOldPrice(view: TextView, value:Double){
    view.text = StringBuilder()
        .append("R$")
        .append(value).toString()
    view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
}

@BindingAdapter("setPrice")
fun setPrice(view: TextView, value:Double){
    view.text = StringBuilder()
        .append("R$")
        .append(value).toString()
}