package com.nurik.desafiomobile.data.pojo

data class Marca(
    val id: Int,
    val nome: String
)