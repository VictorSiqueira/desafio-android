﻿# A aplicação

Aplicativo desenvolvido para teste de competências, deve ser utilizado em dispositivos com a versão de Android 4.4 (API 19) ou superior

# Resultado

## Linha de raciocínio

A construção do aplicativo foi pensada em MVVM, por ser bastante eficiente para dispositivos Android, alem de facilitar bastante o trabalho com listagens, agilizando-as através do databinding, também por garantir a permanência dos dados mesmo que o dispositivo mude sua orientação, garantindo melhor experiência do usuário Foi utilizado o Navigation para organizar o fluxo do app, esse componente também garante uma experiência do usuário consistente e previsível. Optei por não persistir os dados das listagens de produtos e do detalhe do produto, essa decisão foi tomada baseada no fato que não há necessidade de manter os dados, o repositório faz a chamada aos mocks. Caso houvesse algum dado que fosse necessário persistir faria o uso do Room, seguindo a recomendação do Jetpack

## Desafios

Maior desafio encontrado foi realizar os testes instrumentados, não tenho realizado muito desses em projetos que tenho trabalhado, apenas o unitário que fica no diretório test/

## Tempo 

Foram gastas 6h para criação de todos os recursos visuais, e aproximadamente 4h para o modulo de listagem e 5h para o modulo de detalhes. Infelizmente não consegui realizar seguidamente as horas, tive alguns compromissos familiares de final de ano, o que impactou levemente no tempo por não conseguir me manter concentrado no meu raciocínio


