package com.nurik.desafiomobile.data.pojo

data class Marketplace(
    val lojistaPadrao: LojistaPadrao,
    val lojistasEmDestaque: List<LojistasEmDestaque>,
    val maiorPreco: Int,
    val menorPreco: Int
)