package com.nurik.desafiomobile.data.network

import com.nurik.desafiomobile.data.pojo.ProductsResponse
import com.nurik.desafiomobile.data.pojo.Produto
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ViaVarejoAPI {

    @GET("5d1b4f0f34000074000006dd")
    suspend fun getProductList (): Response<ProductsResponse>

    @GET("5d1b4fd23400004c000006e1")
    suspend fun getProductDetail(): Response<Produto>

    @GET("5d1b507634000054000006ed")
    suspend fun getAlsoViewed (): Response<List<Produto>>

    companion object {
        operator fun invoke(): ViaVarejoAPI {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://www.mocky.io/v2/")
                .client(okHttpClient)
                .build()
                .create(ViaVarejoAPI::class.java)

        }
    }
}